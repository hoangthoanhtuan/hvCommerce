﻿namespace Shopcuatoi.Web.Areas.Admin.ViewModels.Products
{
    public class ProductAttributeCombinationVm
    {
        public long AttributeId { get; set; }

        public string AttributeName { get; set; }

        public string Value { get; set; }
    }
}