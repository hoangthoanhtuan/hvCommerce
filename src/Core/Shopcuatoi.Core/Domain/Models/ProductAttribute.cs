﻿using Shopcuatoi.Infrastructure.Domain.Models;

namespace Shopcuatoi.Core.Domain.Models
{
    public class ProductAttribute : Entity
    {
        public string Name { get; set; }
    }
}